﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected List<T> Data { get; set; }

        public InMemoryRepository(List<T> data)
        {
            Data = data;
        }
        
        public Task<List<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task CreateEmployeeAsync(T employee)
        {
            Data.Add(employee);
            return Task.CompletedTask;
        }

        public Task DeleteEmployeeAsync(T employee)
        {
            Data.RemoveAll(x => x.Id == employee.Id);
            return Task.CompletedTask;
        }

        public async Task UpdateEmployeeAsync(T employee)
        {
            if (Data.All(x => x.Id != employee.Id))
                throw new Exception($"Employee with Id = {employee.Id} not found");

            await DeleteEmployeeAsync(employee);
            await CreateEmployeeAsync(employee);
        }
    }
}